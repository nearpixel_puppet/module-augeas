# Description

Installs Augeas.  Also used as a dependency by other Near Pixel puppet modules.

# License

Released under the Apache License, Version 2.0.
